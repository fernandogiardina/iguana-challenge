//
//  iguana_challengeTests.swift
//  iguana-challengeTests
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import XCTest
import Alamofire
import SwiftyJSON

@testable import iguana_challenge

class iguana_challengeTests: XCTestCase {
    
    var expectation:XCTestExpectation?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAllContacts() {
        
        expectation = expectation(description: "\(Resources.urlContacts)")
        
        let viewModel = ContactViewModel(networkClient: NetworkClientAlamofire())
        viewModel.allContacts(success: { [unowned self] (contacts) in
            
            XCTAssert(contacts.count > 0, "No devolvio contactos")
            
            self.expectation?.fulfill()
        }) { (error) in
            XCTAssertTrue(false)
            self.expectation?.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testOneContact() {
        
        expectation = expectation(description: "\(Resources.urlContact)")
        
        let viewModel = ContactViewModel(networkClient: NetworkClientAlamofire())
        let userId = "1004"
        viewModel.contact(id: userId, success: { [unowned self] (contact) in
            
            XCTAssert(contact.userId == userId, "No devolvio el contacto con id 100")
            XCTAssert(contact.addresses?.count == 2, "No tiene dos direcciones")
            
            self.expectation?.fulfill()
        }) { (error) in
            print(error)
            XCTAssertTrue(false)
            self.expectation?.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
}
