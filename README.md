
# README #

## Iguana-challenge ##
- Ejercicio para IguanaFix

## Aspectos Técnicos ##

- xCode v9.2
- Swift 4
- Cocoapods v1.4.0


## Instalación ##
 - pod install --repo-update


## Arquitectura, tecnologías y prácticas ##

 - Arquitectura MVVM
 - Uso de Storyboard
 - Protocolos para inyeccion de dependencias en ViewModel (Datasource)
 - Extensiones en Controllers para implementar delagados y Extensiones aplicadas a String, Data y UIImage necesarias para el proyecto.
 - Libreria externa para el soporte de imagenes SVG
 - Localización (Español, Ingles)
 - Test Unitarios
 - Patrones de diseño: Delegation y Decoration. Al tener un solo tipo de celda no fue necesario usar Strategy ni Custom Delegates
 - El manejo de Errores solo me limite a atajarlo y mostrar el mensaje en caso de ser necesario.
 - Programación funcional. map() para la transformación arrays