//
//  ContactTableViewCell.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import UIKit
import SwiftSVG
import AlamofireImage

class ContactTableViewCell: UITableViewCell {

    static let reuseIdentifier = "ContactTableViewCell"

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: Methods
    func configureImage(url: URL) {
     
        let placeholderImage = UIImage(named: "noimage")?.alpha(0.4)
        
        self.containerView.isHidden = false
        if url.pathExtension == "svg" {
            DispatchQueue.global(qos: .userInitiated).async {
                let sublayer = self.loadSVGImage(url: url)
                DispatchQueue.main.async {
                    self.containerView.addSubview(sublayer)
                }
            }
        }
        else {
            self.containerView.isHidden = true
            thumbImageView.af_setImage(
                withURL: url,
                placeholderImage: placeholderImage,
                filter: CircleFilter(),
                imageTransition: .flipFromBottom(0.1)
            )

        }
        
    }

    func loadSVGImage(url: URL) -> UIView {
        let subLayer = UIView(SVGURL: url) { [unowned self] (svgLayer) in
            svgLayer.resizeToFit(self.containerView.bounds)
        }
        return subLayer
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        thumbImageView.af_cancelImageRequest()
        thumbImageView.layer.removeAllAnimations()
        thumbImageView.image = nil
        
    }
    
}
