//
//  ViewController.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import UIKit
import Toast_Swift
import AlamofireImage

class MainController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let viewModel = ContactViewModel(networkClient: NetworkClientAlamofire())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "contacts".localized()
        
        loadData()
    }
    
    private func loadData() {
        self.view.makeToastActivity(.center)
        viewModel.allContacts(success: { [unowned self] (contacts) in
            if contacts.count > 0 {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.view.hideToastActivity()
                }
            }
            
        }) { (error) in
            self.view.hideToastActivity()
            self.view.makeToast(error, point: self.view.center, title: "Error", image: nil, completion: nil)
        }
    }
    
}

// MARK:  UITableViewDataSource
extension MainController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactTableViewCell.reuseIdentifier, for: indexPath) as! ContactTableViewCell
        
        let contact = viewModel.row(indexPath)
        cell.nameLabel.text = "\(contact.lastName), \(contact.firstName)"
        cell.configureImage(url: URL(string: contact.thumb)!)
        
        return cell
    }
}

// MARK:  UITableViewDelegate
extension MainController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = viewModel.contacts[indexPath.row]
        let controller = storyboard?.instantiateViewController(withIdentifier: DetailController.storyboardId) as! DetailController
        controller.contact = contact
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DeviceType.isiPad ? 150.0 : 100.0
    }
}
