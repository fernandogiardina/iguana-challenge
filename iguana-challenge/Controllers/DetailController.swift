//
//  DetailController.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import UIKit
import Toast_Swift
import AlamofireImage

class DetailController: UIViewController {

    static let storyboardId = "DetailController"
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let viewModel = ContactViewModel(networkClient: NetworkClientAlamofire())
    var contact: Contact! {
        didSet {
            userId = contact.userId
        }
    }
    
    var userId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        initView()
    }
    
    private func loadData() {
        
        navigationItem.hidesBackButton = true
        viewModel.contact(id: userId, success: { [unowned self] (contact) in
            self.contact = contact
            DispatchQueue.main.async {
                self.initView()
            }
        }) { (error) in
            self.view.makeToast(error, point: self.view.center, title: "Error", image: nil, completion: nil)
        }
    }
    
    private func initView() {
        let url = URL(string: contact.photo)!
        let placeholderImage = UIImage(named: "noimage")?.alpha(0.4)
        
        self.activityIndicator.startAnimating()
        
        // Si la imagen no esta disponible dejo el placeholder
        
        photoImageView.af_setImage(
                withURL: url,
                placeholderImage: placeholderImage
        ){ [unowned self] (response) in
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
            }
            
            self.activityIndicator.isHidden = true
            self.navigationItem.hidesBackButton = false
        }
        
        detailLabel.attributedText = viewModel.formatingDetail(contact: contact).html2AttributedString
    }

    deinit {
        print("DetailController deinit")
    }
}
