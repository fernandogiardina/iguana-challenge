//
//  ContactViewModel.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import Foundation

class ContactViewModel: NSObject {
    
    let dataSource: Datasource
    var contacts = [Contact]()
    
    init(networkClient: NetworkClient) {
        dataSource = ContactDatasource(networkClient: networkClient)
        super.init()
    }
    
    func allContacts(success: @escaping ([Contact]) -> Void, failure: @escaping (String) -> Void) {
        dataSource.allContacts(success: { [unowned self] (response) in

            let result = response.array?.map{ item in return Contact(json: item) }
            if let result = result {
                self.contacts = result
            }
            
            success(self.contacts)
            
        }, failure: failure)
    }
    
    func contact(id: String, success: @escaping (Contact) -> Void, failure: @escaping (String) -> Void) {
        dataSource.contact(id: id, success: { (response) in
            let contact = Contact(json: response)
            success(contact)
        }, failure: failure)
    }
    
    func row(_ indexPath: IndexPath) -> Contact {
        return self.contacts[indexPath.row]
    }
 
    func formatingDetail(contact: Contact) -> String {
        let fullNameTitle = "<br/>\("name".localized())"
        let birthDateTitle = "<br/><br/>\("birthdate".localized())"
        let phonesTitle = "<br/><br/>\("phones".localized())"
        let addressesTitle = "<br/><br/>\("addresses".localized())"
        
        let fullNameText = String(format: "<b>%@</b>: %@, %@",fullNameTitle,contact.lastName,contact.firstName)
        let birthdateText = String(format: "<b>%@</b>: %@",birthDateTitle,contact.birthdate)
        
        var phonesText = ""
        for item in contact.phones {
            guard let number = item.number else { continue }
            phonesText = phonesText + String(format: "<div>&nbsp;&nbsp;%@ (%@)</div>", number, item.type.rawValue)
        }
        
        if phonesText.count > 0 {
            phonesText = String(format: "<b>%@</b>: %@", phonesTitle,phonesText)
        }
        
        var addressesText = ""
        if let addresses = contact.addresses {
            for item in addresses {
                addressesText = addressesText + String(format: "<div>&nbsp;&nbsp;%@ (%@)</div>",item.description,item.type.rawValue)
            }
        }
        
        addressesText = String(format: "<b>%@</b>: %@", addressesTitle, addressesText)
        
        let fontSize = DeviceType.isiPad ? 28.0 : 20.0
        return String(format: "<div style='font-size:%2f; font-family: -apple-system'>%@%@%@%@</div>", fontSize ,fullNameText, birthdateText,phonesText,addressesText)
    }
}
