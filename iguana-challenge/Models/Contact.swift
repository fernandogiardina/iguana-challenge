//
//  Contacto.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Contact {
    let userId: String
    let createdAt: String
    let birthdate: String
    let firstName: String
    let lastName: String
    var phones: [Phone]
    let thumb: String
    let photo: String
    var addresses: [Address]?
    
    init(json: JSON) {
        
        userId = json["user_id"].stringValue
        createdAt = json["created_at"].stringValue
        birthdate = json["birth_date"].stringValue
        firstName = json["first_name"].stringValue
        lastName = json["last_name"].stringValue
        
        self.phones = [Phone]()
        if let phones = json["phones"].array {
            
            self.phones = phones.map{item in
                return Phone(type: item["type"].stringValue, number: item["number"].string)
            }
            
        }
        
        thumb = json["thumb"].stringValue
        photo = json["photo"].stringValue
        
        guard let addressesItems = json["addresses"].array else { return }
    
        var addressesArray = [Address]()
        for item in addressesItems {
            for (key, value) in item {
                let address = Address(type: key, description: value.rawValue as! String)
                addressesArray.append(address)
            }
        }
        
        addresses = addressesArray
    }
}



