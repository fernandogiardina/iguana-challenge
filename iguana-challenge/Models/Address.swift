//
//  Address.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import Foundation

enum Addresstype: String {
    case home, work
}

struct Address {
    let type: Addresstype
    let description: String
    
    init(type: String, description: String) {
        self.type = Addresstype(rawValue: type)!
        self.description = description
    }
}
