//
//  Phone.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import Foundation

enum PhoneType: String {
    case Home, Cellphone, Office
}

struct Phone {
    let type: PhoneType
    let number: String?
    
    init(type: String, number: String?) {
        self.type = PhoneType(rawValue: type)!
        self.number = number
    }
}
