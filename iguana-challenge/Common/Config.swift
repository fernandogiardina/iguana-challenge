//
//  Config.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import Foundation

// Rutas
struct Resources {
    static let urlContacts = "https://private-d0cc1-iguanafixtest.apiary-mock.com/contacts"
    static let urlContact = "https://private-d0cc1-iguanafixtest.apiary-mock.com/contacts/%@"
}
