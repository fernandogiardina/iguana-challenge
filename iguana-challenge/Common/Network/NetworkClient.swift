//
//  NetworkClient.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

// Protocolo NetworkClient para inyectar la dependencia.
protocol NetworkClient {
    func post(path: String, parameters: Parameters?, success: @escaping (_ json: JSON) -> (), failure: @escaping (String) -> ())
    func get(path: String, parameters: Parameters?, success: @escaping (JSON) -> (), failure: @escaping (String) -> ())
}

struct NetworkClientAlamofire: NetworkClient {
    
    func get(path: String, parameters: Parameters?, success: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {
        Alamofire.request(path, method: .get, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                switch response.result {
                case .success(let data) :
                    let json = JSON(data)
                    success(json)
                    
                case .failure(let error):
                    failure(error.localizedDescription)
                }
        }
    }
    
    func post(path: String, parameters: Parameters?, success: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {
        Alamofire.request(path, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success(let data) :
                    let json = JSON(data)
                    success(json)
                    
                case .failure(let error):
                    failure(error.localizedDescription)
                }
        }
    }
    
}



