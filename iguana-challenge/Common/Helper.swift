//
//  Helper.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import UIKit

struct DeviceType {
    static let isiPad = UIDevice.current.userInterfaceIdiom == .pad
}
