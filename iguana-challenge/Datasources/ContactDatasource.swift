//
//  ContactDatasource.swift
//  iguana-challenge
//
//  Created by Fernando Giardina on 24/01/2018.
//  Copyright © 2018 Fernando Giardina. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol Datasource {
    var networkClient: NetworkClient { get set }
    func allContacts(success: @escaping (JSON) -> (), failure: @escaping (String) -> ())
    func contact(id: String, success: @escaping (JSON) -> (), failure: @escaping (String) -> ())
}

struct ContactDatasource: Datasource{
 
    var networkClient: NetworkClient
    
    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
    }
    
    func allContacts(success: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {
        self.networkClient.get(path: Resources.urlContacts, parameters: nil, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    func contact(id: String, success: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {
        self.networkClient.get(path: String(format: Resources.urlContact, id), parameters: nil, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
}
